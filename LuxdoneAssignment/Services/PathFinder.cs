﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace LuxdoneAssignment
{

    /// <summary>
    /// <inheritdoc cref="IPathFinderService" />
    /// </summary>
    class PathFinder : IPathFinderService
    {
        private readonly List<Point> _map = new List<Point>();
        public int MaxX { get; set; } = 500;
        public int MaxY { get; set; } = 300;

        /// <inheritdoc />
        public IList<Point>? FindPathBetweenPoints(Point startPoint, Point endPoint)
        {
            Coordinate startCoordinate = new Coordinate(startPoint);
            Coordinate endCoordinate = new Coordinate(endPoint);

            startCoordinate.SetDistance(endCoordinate.X, endCoordinate.Y);

            SortedSet<Coordinate> activeCoordinates = new SortedSet<Coordinate>(new SortedIndexComparer())
            {
                startCoordinate
            };
            ICollection<Coordinate> visitedCoordinates = new List<Coordinate>();

            return FindRoute(endCoordinate, activeCoordinates, visitedCoordinates);
        }

        private IList<Point>? FindRoute(Coordinate finishCoordinate, SortedSet<Coordinate> activeCoordinates, ICollection<Coordinate> visitedCoordinates)
        {
            while (activeCoordinates.Any())
            {
                Coordinate currentCoordinate = activeCoordinates.First();
                bool endPointReached = currentCoordinate.X == finishCoordinate.X && currentCoordinate.Y == finishCoordinate.Y;
                if (endPointReached)
                {
                    Coordinate Coordinate = currentCoordinate;
                    return RecreatePath(Coordinate);
                }

                visitedCoordinates.Add(currentCoordinate);
                activeCoordinates.Remove(currentCoordinate);

                IEnumerable<Coordinate> walkableCoordinates = PossibleCoordinates(_map, currentCoordinate, finishCoordinate);

                foreach (Coordinate walkableCoordinate in walkableCoordinates
                    .Where(y => !visitedCoordinates.Any((x => x.X == y.X && x.Y == y.Y))))
                {
                    Coordinate? existingCoordinate = activeCoordinates
                        .FirstOrDefault(x => x.X == walkableCoordinate.X && x.Y == walkableCoordinate.Y);
                    if (existingCoordinate?.CostDistance > currentCoordinate.CostDistance)
                    {
                        activeCoordinates.Remove(existingCoordinate);
                    }
                    activeCoordinates.Add(walkableCoordinate);
                }

            }
            return null;
        }

        public void AddBlockingCoordinates(IEnumerable<Point> points)
        {
            _map.AddRange(points);
        }

        private IList<Point> RecreatePath(Coordinate? Coordinate)
        {
            IList<Point> drawLine = new List<Point>();
            while (Coordinate != null)
            {
                drawLine.Add(Coordinate.PointCoordinates);
                Coordinate = Coordinate.Parent;
            }
            return drawLine;
        }

        private List<Coordinate> PossibleCoordinates(IList<Point> map, Coordinate currentCoordinate, Coordinate targetCoordinate)
        {
            int newCost = currentCoordinate.Cost + 1;
            (int x, int y) = currentCoordinate;
            List<Coordinate> possiblePathFromCurrentCoordinate = new List<Coordinate>()
            {
                new Coordinate { X = x, Y = y - 1, Parent = currentCoordinate, Cost = newCost },
                new Coordinate { X = x, Y = y + 1, Parent = currentCoordinate, Cost = newCost },
                new Coordinate { X = x - 1, Y = y, Parent = currentCoordinate, Cost = newCost },
                new Coordinate { X = x + 1, Y = y, Parent = currentCoordinate, Cost = newCost },
                new Coordinate { X = x - 1, Y = y +1, Parent = currentCoordinate, Cost = newCost },
                new Coordinate { X = x + 1, Y = y +1, Parent = currentCoordinate, Cost = newCost },
                new Coordinate { X = x - 1, Y = y - 1, Parent = currentCoordinate, Cost = newCost },
                new Coordinate { X = x + 1, Y = y - 1, Parent = currentCoordinate, Cost = newCost },
            };
            possiblePathFromCurrentCoordinate.ForEach(Coordinate => Coordinate.SetDistance(targetCoordinate.X, targetCoordinate.Y));

            return possiblePathFromCurrentCoordinate
                    .Where(Coordinate => Coordinate.X >= 0 && Coordinate.X <= MaxX)
                    .Where(Coordinate => Coordinate.Y >= 0 && Coordinate.Y <= MaxY)
                    .Where(Coordinate => !map.Contains(Coordinate.PointCoordinates))
                    .Where(Coordinate => !map.Contains(new Point(Coordinate.X, Coordinate.Y - 1)))
                    .Where(Coordinate => !map.Contains(new Point(Coordinate.X, Coordinate.Y + 1)))
                    .Where(Coordinate => !map.Contains(new Point(Coordinate.X + 1, Coordinate.Y)))
                    .Where(Coordinate => !map.Contains(new Point(Coordinate.X - 1, Coordinate.Y)))
                    .ToList();
        }

        public void SetMaxCoordinates(int x, int y)
        {
            MaxX = x;
            MaxY = y;
        }
    }
}