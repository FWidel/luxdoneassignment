﻿using System;
using System.Drawing;

/// <summary>
/// Single coordinate, from which we can move on.
/// </summary>
internal class Coordinate
{
    public Point PointCoordinates => new Point(X, Y);
    public int X { get; set; }
    public int Y { get; set; }
    public int Cost { get; set; }
    public int CostDistance => Cost + Distance;
    public Coordinate? Parent { get; set; }
    private int Distance { get; set; }

    /// <summary>
    /// Initializes a new instance of the <see cref="Coordinate"/> class.
    /// </summary>
    /// <param name="point">Point.</param>
    public Coordinate(Point point)
    {
        X = point.X;
        Y = point.Y;
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="Coordinate"/> class.
    /// </summary>
    public Coordinate() { }

    /// <summary>
    /// Set distance between two points.
    /// </summary>
    /// <param name="targetX">Target point X.</param>
    /// <param name="targetY">Target point Y.</param>
    public void SetDistance(int targetX, int targetY)
    {
        Distance = Math.Abs(targetX - X) + Math.Abs(targetY - Y);
    }

    internal void Deconstruct(out int x, out int y)
    {
        x = X;
        y = Y;
    }
}

