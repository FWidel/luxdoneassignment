﻿using System.Collections.Generic;
using System.Drawing;

namespace LuxdoneAssignment
{
    /// <summary>
    /// Find the path between two points that do not intersect with other lines.
    /// </summary>
    public interface IPathFinderService
    {
        /// <summary>
        /// Find Path between two points.
        /// </summary>
        /// <param name="a">Start point(x,y).</param>
        /// <param name="b">End point(x,y).</param>
        /// <returns>Path between two points.</returns>
        IList<Point>? FindPathBetweenPoints(Point a, Point b);
        void SetMaxCoordinates(int x, int y);

        /// <summary>
        /// Add points that arleady exists in other paths.
        /// </summary>
        /// <param name="points"></param>
        void AddBlockingCoordinates(IEnumerable<Point> points);
    }
}