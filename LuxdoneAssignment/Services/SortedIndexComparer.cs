﻿using System.Collections.Generic;

internal class SortedIndexComparer : IComparer<Coordinate>
{
    public int Compare(Coordinate x, Coordinate y)
    {
        return x.CostDistance.CompareTo(y.CostDistance);
    }
}
