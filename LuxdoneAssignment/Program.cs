using Microsoft.Extensions.DependencyInjection;
using System;
using System.Windows.Forms;

namespace LuxdoneAssignment
{
    internal static class Program
    {
        /// <summary>
        /// Service Provider.
        /// </summary>
        public static IServiceProvider ServiceProvider { get; set; }

        static void ConfigureServices()
        {
            var services = new ServiceCollection();
            services.AddSingleton<IPathFinderService, PathFinder>();
            ServiceProvider = services.BuildServiceProvider();
        }

        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            // ApplicationConfiguration.Initialize();
            ConfigureServices();
            Application.Run(new LuxdoneAssignment());
        }
    }
}
