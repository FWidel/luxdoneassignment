﻿using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace LuxdoneAssignment
{
    public partial class LuxdoneAssignment : Form
    {
        private Point p1;
        private readonly IList<IList<Point>> Lines = new List<IList<Point>>();
        private readonly IPathFinderService _pathFinderService;
        private static readonly int MaxX = 300;
        private static readonly int MaxY = 200;

        /// <summary>
        /// LuxdoneAssignment.
        /// </summary>
        public LuxdoneAssignment()
        {
            InitializeComponent();
            _pathFinderService = (IPathFinderService)Program.ServiceProvider.GetService(typeof(IPathFinderService));

            // The algorithm is not very optimized, so for a larger area, it would take a long time to calculate it correctly ;)
            _pathFinderService.SetMaxCoordinates(x: MaxX, y: MaxY);
        }

        private void Panel1_MouseDown(object sender, MouseEventArgs e)
        {
            HandleNewPoint(e.Location);
            Refresh();
        }

        private void HandleNewPoint(Point point)
        {
            if (p1.IsEmpty)
            {
                p1 = point;
            }
            else
            {
                IList<Point>? foundPath = _pathFinderService.FindPathBetweenPoints(p1, point);
                if (foundPath is null)
                {
                    return;
                }
                Lines.Add(foundPath);
                _pathFinderService.AddBlockingCoordinates(foundPath);
                p1 = new Point();
            }
        }

        private void Panel1_Paint(object _, PaintEventArgs e)
        {
            PaintDot(e.Graphics);
            DrawLines(e.Graphics);
        }

        private void DrawLines(Graphics graphics)
        {
            using Pen pen = new Pen(Color.Blue, 1);
            foreach (var line in Lines)
            {
                for (int i = 0; i < line.Count - 1; i++)
                {
                    graphics.DrawLine(pen, line[i], line[i + 1]);
                }
            }
        }

        private void PaintDot(Graphics graphics)
        {
            Brush aBrush = Brushes.Blue;
            graphics.FillRectangle(aBrush, p1.X, p1.Y, 1, 1);
        }
    }
}


